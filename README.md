# banijipers
<!-- file README.md -->
An experimental multi-threaded persistent system for Linux/x86-64
using [libgccjit](https://gcc.gnu.org/onlinedocs/jit/) for
"just-in-time" code generation and [Ravenbrook
MPS](https://www.ravenbrook.com/project/mps) for multi-threaded
garbage collection.

The persistent store is in some `pstorebnj/` [sub-]directory. That
directory should contain a `pstorebnj/+MANIFEST.bnji` textual file
describing the set of files making that persistent store and how and
in what order they should be loaded. The `+` sign in front of
`MANIFEST` ensures in practice that such a manifest file is listed
alphanumerically first (by `ls` or shell globbing). Persisting in
some textual format makes the persistent store version control friendly
(e.g. using [git](http://git-scm.com/)...).

Notice that several existing systems have [application
checkpointing](https://en.wikipedia.org/wiki/Application_checkpointing)
facilities. In particular, [SBCL](http://sbcl.org/) has a
[`save-lisp-and-die`](http://www.sbcl.org/manual/#Saving-a-Core-Image)
primitive. However, the "core image" is not textual, it is tied to a
*specific* version of SBCL, and does not persist several threads (or
an agenda of tasklets).

At high-level, *banijipers* is a dynamically-typed system. Everything
is a *value*, which is represented as a `void*` generic pointer (so
also as a `intptr_t` ...), that is a 64 bit word. When a value is a
pointer, it is word aligned (so the last three least significant bits
are 0).

As in most language implementations (e.g. Lisp like languages, Ocaml,
etc..) a value can be a tagged integer. So a value *V* represented
with a word whose least significant bit -LSB- is 1 is the integer
*V*/2. Hence our integers fit in 63 bits.

A value could be nil, that is either the `NULL` pointer or some
pointer (like `(void*)((char*)8)`, which we name `EMPTY_BNJI`) whose
61 most significant bits are 0. Perhaps we could need the `EMPTY_BNJI`
in the *implementation* of some hashtables, for slots which have been
cleared (but did carry previously some genuine value).


## Values

Most **values** (sometimes called *first-class* values) are immutable,
with an important exception: *objects* (to be covered later). Our
immutable non-nil values should include:

* tagged integers

* reference to some mutable object

* UTF-8 strings

* sequence of arbitrary bytes

* sequence of 32 bits integers

* sequence of 64 bits integers

* sequence of 64 bits `double`

* tuples of object references (or nil)

* ordered set of object references. So membership is testable dichotomically.

* immutable instances (to be explained below). Some of them may
  represent code chunks (to be transformed by libgccjit).
  
* closures

* etc...

Values are hashed (there is a function to compute their hash-code, a
non-zero 32 bits integer) and ordered (two values can *always* be
compared).

Of course, values are persistent.

## Quasi-values

**Quasi-values** include *values* as defined above, and
*garbage-collected memory zones* (which cannot be returned as
values). For example, some objects might contain internally some
hash-table, and the buckets of that hash-table are (internally
mutable) memory zones known to the GC but are not first-class
values. So they are quasi-values. Likewise, we probably need some
red-black trees to implemented ordered collections, but the node of
such RB-trees are quasi-values.

Quasi-values are not persistent by themselves (unless they happen to
be first-class values), but some persistence routines are able to use
them.

## Instances

To be explained more.

An instance has a class (which is an object) and a sequence of
component values. Since the instance is immutable, neither the class
nor the component values can change after the creation of that
instance.

Think of the runtime type of an instance as something similar to C
`struct`-s, perhaps ending with some [flexible array
member](https://en.wikipedia.org/wiki/Flexible_array_member). But all
the fields of that `struct` are conceptually read-only (so fixed at
creation time). The "visible" fields of that `struct` are all
read-only values (and we call them components).

## Objects

To be explained.

## Agenda

Related to our worker threads. To be defined. Our worker threads (we
run a fixed number of them) should be tuned to allocate and garbage
collect efficiently. Other threads (support threads, including HTTP
service threads) will allocate slowly, probably with a global mutex
(inspired by [global interpreter
lock](https://en.wikipedia.org/wiki/Global_interpreter_lock) of
popular language implementation)... However, the worker threads are
running and allocating concurrently.

## Code representation and generation with `libgccjit`

To be explained. We'll have to use some instances (of *predefined*
classes) representing the code (basically nearly C-like).

## Garbage-collection

It is MPS-based and is complex enough to be explained in another
file. So see our [MPS GC](MPS-GC.md) page.

See also [this `softwarerecs` question](https://softwarerecs.stackexchange.com/q/2954/1877).

# ABI compatibility
If we code in Rust we are deeply concerned by ABI compatibility with C (notably because of MPS).

See [Notes on Type Layouts and ABIs in Rust](https://gankro.github.io/blah/rust-layouts-and-abis/)
