/* 
    BANIJIPERS - file main.rs

    Copyright © 2019 Niklas Rosencrantz & Basile Starynkevitch

    This Banijipers program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
extern crate clap;
extern crate time;
extern crate xorshift;

use std::fmt;
use time::precise_time_ns;
use xorshift::{Rng, SeedableRng, Xorshift128};
use clap::{Arg, App, SubCommand};

fn main() {
    let git_hash = format!("Git Hash: {}", env!("GIT_HASH"));
    let matches = App::new("Banijipers")
                          .version(env!("GIT_HASH"))
                          .author("Basile S <basile@starynkevitch.net>:Niklas R<niklasro@gmail.com>")
                          .about(env!("GIT_HASH"))
                          .arg(Arg::with_name("config")
                               .short("c")
                               .long("config")
                               .value_name("FILE")
                               .help("Sets a custom config file")
                               .takes_value(true))
                          .arg(Arg::with_name("output-random-objid")
                               .short("objid")
                               .long("output-random-objid")
                               .help("Outputs random object id")
                               .takes_value(false))
                          .arg(Arg::with_name("INPUT")
                               .help("Sets the input file to use")
                               .required(false)
                               .index(1))
                          .arg(Arg::with_name("v")
                               .short("v")
                               .multiple(true)
                               .help("Sets the level of verbosity"))
                          .subcommand(SubCommand::with_name("output-random-objid")
                                      .about("controls random objid")
                                      .version("0.1.0")
                                      .author("Basile S. <basile@starynkevitch.net>")
                                      .arg(Arg::with_name("debug")
                                          .short("d")
                                          .help("print debug information verbosely")))
                          .get_matches();


    ///@@TODO: seed from /dev/random
    // Use the high-resolution performance counter for seeding
    let now = precise_time_ns();
    // Manually seed a Xorshift128+ PRNG
    let states = [now, now];
    let mut rng: Xorshift128 = SeedableRng::from_seed(&states[..]);
    let mut str = String::from("_");
    str.push_str(&rng.gen_ascii_chars().take(20).collect::<String>());
    println!("Random object id -> {}", str);
}
