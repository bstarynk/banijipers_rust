/* 
    BANIJIPERS - file lib.rs

    Copyright © 2019 Niklas Rosencrantz & Basile Starynkevitch

    This Banijipers program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
extern crate gccjit;

use gccjit::Context;
use gccjit::FunctionType;

use std::default::Default;
use std::mem;


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
    #[test]
    fn works2() {
    let context = gccjit::Context::default();
    let void_ty = context.new_type::<()>();
    let fun = context.new_function(None,
                                   gccjit::FunctionType::Exported,
                                   void_ty,
                                   &[],
                                   "hello",
                                   false);
    let block = fun.new_block("main_block");
    let function_ptr = context.new_function_pointer_type(None,
                                                         void_ty,
                                                         &[],
                                                         false);
    let ptr = unsafe {
        context.new_rvalue_from_ptr(function_ptr, say_hello as *mut ())
    };
    let call = context.new_call_through_ptr(None, ptr, &[]);
    block.add_eval(None, call);
    block.end_with_void_return(None);
    let result = context.compile();
    let hello = result.get_function("hello");
    let hello_fn : extern "C" fn() =
        if !hello.is_null() {
            unsafe { std::mem::transmute(hello) }
        } else {
            panic!("failed to retrieve function");
        };
    hello_fn();
}

extern "C" fn say_hello() {
    println!("hello, world!");
}

}
