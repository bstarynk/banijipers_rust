### Why we cannot use Go no matter how much we want

The simple reason why we cannot use Go is that Go generates too large code due to static linking and including the runtime in plugins. If these issues were fixed (see https://stackoverflow.com/questions/54509223) then we might reconsider but probably not because we are already getting started with another choice of implementation. 